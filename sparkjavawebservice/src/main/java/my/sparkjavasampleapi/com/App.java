package my.sparkjavasampleapi.com;

import static spark.Spark.*;
import com.google.common.hash.*;
import java.nio.charset.StandardCharsets;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        post("/hash", "application/json", (request, response) -> {
            JSONParser parser = new JSONParser();
            String s = request.body();
            Object parsedObj = parser.parse(s);
            String textValue = ((JSONObject) parsedObj).get("text").toString();

            response.type("application/json");
            response.status(200);
            return new Hash(Hashing.sha256()
                    .hashString(textValue, StandardCharsets.UTF_8)
                    .toString());
        }, new JsonTransformer());
    }
}